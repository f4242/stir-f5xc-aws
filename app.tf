provider "helm" {
  kubernetes {
    config_path = format("%s/_output/kubeconfig", path.root)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
      command     = "aws"
    }
  }
}

resource "local_file" "this_kubeconfig" {
#  depends_on = [volterra_discovery.eks]
  content    = base64decode(local.kubeconfig_b64)
  filename   = format("%s/_output/kubeconfig", path.root)
}


resource "helm_release" "stir" {
  depends_on = [local_file.this_kubeconfig]
  name       = "stir"
  repository = "https://gitlab.com/api/v4/projects/35299821/packages/helm/stable"
  chart      = "thehive"
  version    = "0.7.0"
  timeout    = 900
  wait       = true
  values = [
    "${file("./values.yaml")}"
  ]
}

resource "kubernetes_config_map" "http2logstash-api-p12-file" {
  for_each    = toset(var.xc_waap_http_pull ? [var.skg_name] : [])
  depends_on  = [helm_release.stir]
  metadata {
    name = "http2logstash-api-p12-file"
  }
  binary_data = {
    api_p12_file = "${filebase64(var.api_p12_file)}"
  }
}

resource "kubernetes_config_map" "http2logstash-config" {
  for_each    = toset(var.xc_waap_http_pull ? [var.skg_name] : [])
  depends_on  = [helm_release.stir]
  metadata {
    name = "http2logstash-config"
  }
  data = {
    api_url = var.api_url
    api_p12_password = var.api_p12_password
    app_namespace = var.app_namespace
  }
}

resource "kubernetes_deployment" "http2logstash" {
  for_each    = toset(var.xc_waap_http_pull ? [var.skg_name] : [])
  depends_on  = [helm_release.stir]
  metadata {
    name = "stir-http2logstash"
    labels = {
      test = "http2logstash"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "http2logstash"
      }
    }
    strategy {
      type = "Recreate"
    }
    template {
      metadata {
        labels = {
          app = "http2logstash"
        }
      }

      spec {
        init_container {
          name = "wait-for-webserver"
          image = "curlimages/curl:latest"
          command = tolist(["/bin/sh","-c"])
          args = tolist(["while [ $(curl -sw '%%{http_code}' -k -u elastic:Q3mj0YKThTpdKVSj7OrP http://elasticsearch-master:9200 -o /dev/null) -ne 200 ]; do sleep 5; echo 'Waiting for the webserver...'; done"])
        }
        container {
          image = "registry.gitlab.com/f518/salient-http2logstash:latest"
          name  = "http2logstash"
          env {
            name = "poll_freq_seconds"
            value = "30"
          }
          env {
            name = "log_interval_seconds"
            value = "60"
          }
          volume_mount {
            name = "http2logstash-api-p12-file"
            mount_path = "/certificates"
          }
          volume_mount {
            name = "http2logstash-config"
            mount_path = "/config"
          }
        }
        restart_policy = "Always"
        volume {
          name = "http2logstash-api-p12-file"
          config_map {
            name = "http2logstash-api-p12-file"
            items {
              key = "api_p12_file"
              path = "api-creds.p12"
            }
          }
        }
        volume {
          name = "http2logstash-config"
          config_map {
            name = "http2logstash-config"
            items {
              key = "api_url"
              path = "api_url"
            }
            items {
              key = "api_p12_password"
              path = "api_p12_password"
            }
            items {
              key = "app_namespace"
              path = "app_namespace"
            }
          }
        }
      }
    }
  }
}


resource "volterra_discovery" "eks" {
  for_each    = toset(var.eks_only ? [] : [var.skg_name])
  name        = var.skg_name
  description = "Discovery object to discover all services in eks cluster"
  namespace   = "system"
  depends_on  = [module.eks, helm_release.stir]

  where {
    site {
      ref {
        name      = var.skg_name
        namespace = "system"
      }
      network_type = "VIRTUAL_NETWORK_SITE_LOCAL_INSIDE"
    }
  }
  discovery_k8s {
    access_info {
      kubeconfig_url {
        secret_encoding_type = "EncodingNone"
        clear_secret_info {
          url = format("string:///%s", local.kubeconfig_b64)
        }
      }
      reachable = false
      isolated  = true
    }
    publish_info {
      disable = true
    }
  }
}
