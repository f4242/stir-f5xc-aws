data "volterra_namespace" "this" {
  count = var.volterra_namespace_exists && !var.eks_only ? 1 : 0
  name  = var.volterra_namespace
}

resource "volterra_namespace" "this" {
  count = var.volterra_namespace_exists || var.eks_only ? 0 : 1
  name  = var.volterra_namespace
}

resource "volterra_service_policy" "allow-src-ip" {
  name      = "allow-src-ip"
  namespace = local.namespace
  algo      = "FIRST_MATCH"

  // One of the arguments from this list "deny_list rule_list legacy_rule_list allow_all_requests deny_all_requests internally_generated allow_list" must be set
  allow_list {
    prefix_list {
      prefixes = var.allow_src_ip
    }
    default_action_deny = true
  } 
  // One of the arguments from this list "any_server server_name server_selector server_name_matcher" must be set
  any_server = true
}

resource "volterra_origin_pool" "stir-kibana" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-kibana-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Kibana k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-kibana.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 5601
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_discovery.eks, volterra_service_policy.allow-src-ip]
}

resource "volterra_origin_pool" "stir-misp-web" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-misp-web-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to MISP-Web k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-misp-web.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 8081
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-kibana]
}

resource "volterra_origin_pool" "stir-cortex" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-cortex-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Cortex k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-cortex.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 9001
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-misp-web]
}

resource "volterra_origin_pool" "stir-logstash-http" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-logstash-http-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Logstash HTTP-Input k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-logstash.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 5044
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-cortex]
}

resource "volterra_origin_pool" "stir-logstash-syslog" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-logstash-syslog-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Logstash Syslog-Input k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-logstash.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 5144
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-logstash-http]
}

resource "volterra_origin_pool" "stir-logstash-beats" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-logstash-beats-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Logstash Beats-Input k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-logstash.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 5244
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-logstash-syslog]
}

resource "volterra_origin_pool" "stir-node-red" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-node-red-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to Node-Red k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-node-red.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 1880
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-logstash-beats]
}

resource "volterra_origin_pool" "stir-thehive" {
  for_each               = toset(var.eks_only ? [] : [var.skg_name])
  name                   = "stir-thehive-pool"
  namespace              = local.namespace
  description            = format("Origin pool pointing to TheHive k8s service running on RE's")
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      inside_network  = true
      outside_network = false
      vk8s_networks   = false
      service_name    = "stir-thehive.default"
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this[each.key].name
          namespace = "system"
        }
      }
    }
  }
  port               = 9000
  no_tls             = true
  endpoint_selection = "LOCAL_PREFERRED"
  depends_on = [volterra_origin_pool.stir-node-red]
}

#resource "volterra_waf" "this" {
#  for_each    = toset(var.eks_only ? [] : [var.skg_name])
#  name        = format("%s-waf", var.skg_name)
#  description = format("WAF in block mode for %s", var.skg_name)
#  namespace   = local.namespace
#  app_profile {
#    cms       = []
#    language  = []
#    webserver = []
#  }
#  mode = "BLOCK"
#  lifecycle {
#    ignore_changes = [
#      app_profile
#    ]
#  }
#}

resource "volterra_http_loadbalancer" "stir-kibana" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-kibana-lb"
  namespace                       = local.namespace
  description                     = format("HTTPS loadbalancer object for Kibana origin server")
  domains                         = [var.kibana_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-kibana[each.key].name
#      namespace = local.namespace
#    }
#  }

  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }

#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-kibana[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }

  active_service_policies {
    policies {
      namespace = local.namespace
      name = "allow-src-ip"
    } 
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
  depends_on = [volterra_origin_pool.stir-thehive]
}

resource "volterra_http_loadbalancer" "stir-misp-web" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-misp-web-lb"
  namespace                       = local.namespace
  description                     = format("HTTPS loadbalancer object for MISP-Web origin server")
  domains                         = [var.misp_web_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-misp-web[each.key].name
#      namespace = local.namespace
#    }
#  }

  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }

#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-misp-web[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }

  active_service_policies {
    policies {
      namespace = local.namespace
      name = "allow-src-ip"
    }
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
  depends_on = [volterra_http_loadbalancer.stir-kibana]
}

resource "volterra_http_loadbalancer" "stir-cortex" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-cortex-lb"
  namespace                       = local.namespace
  description                     = format("HTTPS loadbalancer object for Cortex origin server")
  domains                         = [var.cortex_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-cortex[each.key].name
#      namespace = local.namespace
#    }
#  }

  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }

#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-cortex[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }

  active_service_policies {
    policies {
      namespace = local.namespace
      name = "allow-src-ip"
    }
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
  depends_on = [volterra_http_loadbalancer.stir-misp-web]
}

resource "volterra_http_loadbalancer" "stir-logstash-http" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-logstash-http-lb"
  namespace                       = local.namespace
  description                     = format("TCP loadbalancer object for Logstash HTTP Input origin server")
  domains                         = [var.logstash_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-logstash[each.key].name
#      namespace = local.namespace
#    }
#  }
  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }
#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-logstash-http[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
    }
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true    
  no_challenge                    = true
  depends_on = [volterra_http_loadbalancer.stir-cortex]
}

resource "volterra_tcp_loadbalancer" "stir-logstash-syslog" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-logstash-syslog-lb"
  namespace                       = local.namespace
  description                     = format("TCP loadbalancer object for Logstash Syslog input origin server")
  domains                         = [var.logstash_app_domain]
  dns_volterra_managed            = true
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-logstash[each.key].name
#      namespace = local.namespace
#    }
#  }
#  https_auto_cert {
#    add_hsts      = var.enable_hsts
#    http_redirect = var.enable_redirect
#    no_mtls       = true
#  }
#  http {
#    dns_volterra_managed = true
#  }
#
#  routes {
#    simple_route {
#      path {
#        regex = ".*"
#      }
#      origin_pools {
#        pool {
#          name = volterra_origin_pool.stir-logstash[each.key].name
#          namespace = local.namespace
#        }
#      }
#      disable_host_rewrite = true
#    }
#  }

#  disable_waf                     = true
#  disable_rate_limit              = true
#  round_robin                     = true
#  service_policies_from_namespace = true
  listen_port                     = 5144
  origin_pools_weights {
      pool {
        name = volterra_origin_pool.stir-logstash-syslog[each.key].name
        namespace = local.namespace
      }
  }
    
#  no_challenge                    = true
  depends_on = [volterra_http_loadbalancer.stir-logstash-http]
}

resource "volterra_tcp_loadbalancer" "stir-logstash-beats" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-logstash-beats-lb"
  namespace                       = local.namespace
  description                     = format("TCP loadbalancer object for Logstash Beats input origin server")
  domains                         = [var.logstash_app_domain]
  dns_volterra_managed            = true
  advertise_on_public_default_vip = true
  listen_port                     = 5244
  origin_pools_weights {
      pool {
        name = volterra_origin_pool.stir-logstash-beats[each.key].name
        namespace = local.namespace
      }
  }
  depends_on = [volterra_tcp_loadbalancer.stir-logstash-syslog]
}


resource "volterra_http_loadbalancer" "stir-node-red" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-node-red-lb"
  namespace                       = local.namespace
  description                     = format("HTTPS loadbalancer object for Node-Red origin server")
  domains                         = [var.node_red_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-node-red[each.key].name
#      namespace = local.namespace
#    }
#  }

  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }

#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-node-red[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true

      advanced_options {
        web_socket_config {
          use_websocket = true
        }
      }
    }
  }

  active_service_policies {
    policies {
      namespace = local.namespace
      name = "allow-src-ip"
    }
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
  depends_on = [volterra_tcp_loadbalancer.stir-logstash-beats]
}


resource "volterra_http_loadbalancer" "stir-thehive" {
  for_each                        = toset(var.eks_only ? [] : [var.skg_name])
  name                            = "stir-thehive-lb"
  namespace                       = local.namespace
  description                     = format("HTTPS loadbalancer object for TheHive origin server")
  domains                         = [var.thehive_app_domain]
  advertise_on_public_default_vip = true
#  default_route_pools {
#    pool {
#      name      = volterra_origin_pool.stir-thehive[each.key].name
#      namespace = local.namespace
#    }
#  }

  https_auto_cert {
    add_hsts      = var.enable_hsts
    http_redirect = var.enable_redirect
    no_mtls       = true
  }

#  http {
#    dns_volterra_managed = true
#  }

  routes {
    simple_route {
      path {
        regex = ".*"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.stir-thehive[each.key].name
          namespace = local.namespace
        }
      }
      disable_host_rewrite = true
      advanced_options {
        # Needed for GET /api/stream/XXXXXXX which has a >60s latency on TheHive server
        timeout = 90000
      }
    }
  }

  more_option {
    # Needed for GET /api/stream/XXXXXXX which has a >60s latency on TheHive server
    idle_timeout = 90000
  }

  active_service_policies {
    policies {
      namespace = local.namespace
      name = "allow-src-ip"
    }
  }

  disable_waf                     = true
  disable_rate_limit              = true
  round_robin                     = true
  service_policies_from_namespace = true
  no_challenge                    = true
  depends_on = [volterra_http_loadbalancer.stir-node-red]
}
