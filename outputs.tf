output "kubeconfig_filename" {
  description = "EKS kubeconfig file name"
  value       = module.eks.kubeconfig_filename
}

output "kibana_app_url" {
  description = "Domain VIP to access Kibana deployed on EKS"
  value       = var.kibana_app_domain != "" ? format("https://%s", var.kibana_app_domain) : ""
}

output "misp_web_app_url" {
  description = "Domain VIP to access MISP Web deployed on EKS"
  value       = var.misp_web_app_domain != "" ? format("https://%s", var.misp_web_app_domain) : ""
}

output "cortex_app_url" {
  description = "Domain VIP to access Cortex deployed on EKS"
  value       = var.cortex_app_domain != "" ? format("https://%s", var.cortex_app_domain) : ""
}

output "logstash_app_url" {
  description = "Domain VIP to access Logstash deployed on EKS"
  value       = var.logstash_app_domain != "" ? format("%s:5044", var.logstash_app_domain) : ""
}

output "node_red_app_url" {
  description = "Domain VIP to access Node-Red deployed on EKS"
  value       = var.node_red_app_domain != "" ? format("https://%s", var.node_red_app_domain) : ""
}

output "thehive_app_url" {
  description = "Domain VIP to access TheHive deployed on EKS"
  value       = var.thehive_app_domain != "" ? format("https://%s", var.thehive_app_domain) : ""
}
