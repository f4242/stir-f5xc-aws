# stir-f5xc-aws


This is a Terraform module used to deploy the [STIR Helm chart](https://gitlab.com/f4242/stir-helm-chart) on Amazon's AWS and expose the STIR services through F5's Distributed Cloud platform. This module is based on F5's Distributed Cloud (formerly Volterra) [Secure Kubernetes Gateway Terraform module](https://github.com/volterraedge/terraform-volterra-secure-k8s-gateway). Read the [Secure Kubernetes Gateway usecase guide](https://volterra.io/docs/quick-start/secure-kubernetes-gateway) to learn more. Optionally, you can integrate with F5 Distributed Cloud WAAP by polling for security alerts and importing them to STIR.

---

## Overview

### High-level diagram for base deployment
![High-level diagram for base deployment](./STIR_base_deployment.png)

### High-level diagram for integration with F5 Distributed Cloud WAAP
![High-level diagram for integration with F5 Distributed Cloud WAAP](./STIR_F5XC_WAAP_integration.png)

---

## Prerequisites:

### AWS Account

* AWS Programmatic access credentials

  You should already have a user create in AWS account and have already have [aws programmatic access credentials](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) for the user.

* AWS IAM Policy for the user

  Follow this [link](https://volterra.io/docs/reference/cloud-cred-ref/aws-vpc-cred-ref) to add permission for AWS IAM user. You may need to contact your IAM admin to do this.

### Volterra Account

* Signup For Volterra Account

  If you don't have a Volterra account. Please follow this link to [signup](https://console.ves.volterra.io/signup/)

* Download Volterra API credentials file

  Follow [how to generate API Certificate](https://volterra.io/docs/how-to/user-mgmt/credentials) to create API credentials

* Setup domain delegation

  Follow steps from this [link](https://volterra.io/docs/how-to/app-networking/domain-delegation) to create domain delegation.

### Command Line Tools

* Install terraform

  For homebrew installed on macos, run below command to install terraform. For rest of the os follow the instructions from [this link](https://learn.hashicorp.com/tutorials/terraform/install-cli) to install terraform

  ```bash
  $ brew tap hashicorp/tap
  $ brew install hashicorp/tap/terraform

  # to update
  $ brew upgrade hashicorp/tap/terraform
  ```

* Install Kubectl

  Please follow this [doc](https://kubernetes.io/docs/tasks/tools/install-kubectl/) to install kubectl

* Install aws-iam-authenticator

  Please follow this [doc](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html) to install aws-iam-authenticator

* Export the API certificate password as environment variable, this is needed for volterra provider to work
  ```bash
  export VES_P12_PASSWORD=<your credential password>
  ```
* If using the optional F5 XC WAAP integration, also specify the API certificate password as a variable in the terraform script (see sample below), in addition to exporting it as an environment variable in the preceding step
  ```hcl
  variable "api_p12_password" {
    default = "<your credential password>"
  }
  ```
---

## Usage Example
### Create a file (for example "main.tf") containing the code below and execute "terraform init", "terraform plan" and "terraform apply" in the same folder
### This example shows a completely automated scenario, where all F5 Distributed Cloud and AWS EKS objects are created by the module and F5 Distributed Cloud WAAP integration is enabled

```hcl
variable "api_url" {
  #--- UNCOMMENT FOR TEAM OR ORG TENANTS
  # default = "https://<TENANT-NAME>.console.ves.volterra.io/api"
  #--- UNCOMMENT FOR INDIVIDUAL/FREEMIUM
  # default = "https://console.ves.volterra.io/api"
}

# This points the absolute path of the api credentials file you downloaded from Volterra
variable "api_p12_file" {
  default = "path/to/your/api-creds.p12"
}

variable "api_p12_password" {
  default = ""
}
# Below is an option to pass access key and secret key as you probably don't want to save it in a file
# Use env variable before you run `terraform apply` command
# export TF_VAR_aws_access_key=<your aws access key>
# export TF_VAR_aws_secret_key=<your aws secret key>
variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_region" {
  default = "us-east-2"
}

variable "aws_az" {
  default = "us-east-2a"
}

variable "app_namespace" {
  default = "hipster-shop"
}

variable "siem_namespace" {
  default = "stir"
}

variable "name" {
  default = "stir-skg-aws"
}

variable "kibana_app_fqdn" {
  default = "kibana.stir.sr.f5-demo.com"
}

variable "misp_web_app_fqdn" {
  default = "misp.stir.sr.f5-demo.com"
}

variable "cortex_app_fqdn" {
  default = "cortex.stir.sr.f5-demo.com"
}

variable "logstash_app_fqdn" {
  default = "logstash.stir.sr.f5-demo.com"
}

variable "node_red_app_fqdn" {
  default = "node-red.stir.sr.f5-demo.com"
}

variable "thehive_app_fqdn" {
  default = "thehive.stir.sr.f5-demo.com"
}


# This is the VPC CIDR for AWS
variable "aws_vpc_cidr" {
  default = "192.168.0.0/22"
}

# Map to hold different CE CIDR, if you are not using default aws_vpc_cidr then you need to change the below map as well
variable "aws_subnet_ce_cidr" {
  default = {
    "outside"  = "192.168.0.0/25"
    "inside"   = "192.168.0.192/26"
    "workload"   = "192.168.0.128/26"
  }
}

# Map to hold different EKS cidr with key as desired AZ on which the subnet should exist
variable "aws_subnet_eks_cidr" {
  default = {
    "us-east-2a" = "192.168.1.0/25"
    "us-east-2b" = "192.168.1.128/25"
  }
}

variable "allow_tls_prefix_list" {
  type        = list(string)
  description = "Allow TLS prefix list"
  default     = ["gcr.io", "storage.googleapis.com", "docker.io", "docker.com", "amazonaws.com", "gitlab.com", "elastic.co", "gitlab-static.net", "console.ves.volterra.io", "registry.npmjs.org", "sendinblue.com"]
}

variable "allow_src_ip" {
  type        = list(string)
  description = "Allow source IP prefix list"
  default     = ["0.0.0.0/0"]
}

locals{
  namespace = var.siem_namespace != "" ? var.siem_namespace : var.name
}

terraform {
  required_providers {
    volterra = {
      source = "volterraedge/volterra"
      version = "0.4.0"
    }
  }
}

provider "volterra" {
  api_p12_file = var.api_p12_file
  url          = var.api_url
}

module "stir" {
  source                    = "git::https://gitlab.com/f4242/stir-f5xc-aws/"
  skg_name                  = var.name
  volterra_namespace        = local.namespace
  volterra_namespace_exists = true
  kibana_app_domain         = var.kibana_app_fqdn
  misp_web_app_domain       = var.misp_web_app_fqdn
  cortex_app_domain         = var.cortex_app_fqdn
  logstash_app_domain       = var.logstash_app_fqdn
  node_red_app_domain       = var.node_red_app_fqdn
  thehive_app_domain        = var.thehive_app_fqdn
  api_url                   = var.api_url
  api_p12_file              = var.api_p12_file
  api_p12_password          = var.api_p12_password
  app_namespace             = var.app_namespace
  aws_secret_key            = var.aws_secret_key
  aws_access_key            = var.aws_access_key
  aws_region                = var.aws_region
  aws_az                    = var.aws_az
  aws_vpc_cidr              = var.aws_vpc_cidr
  aws_subnet_ce_cidr        = var.aws_subnet_ce_cidr
  aws_subnet_eks_cidr       = var.aws_subnet_eks_cidr
  allow_tls_prefix_list     = var.allow_tls_prefix_list
  eks_only                  = false
  xc_waap_http_pull         = true
  allow_src_ip              = var.allow_src_ip 
}

output "kubeconfig_filename" {
  value = module.stir.kubeconfig_filename
}

output "kibana_app_url" {
  value = module.stir.kibana_app_url
}

output "misp_web_app_url" {
  value = module.stir.misp_web_app_url
}

output "cortex_app_url" {
  value = module.stir.cortex_app_url
}

output "logstash_app_url" {
  value = module.stir.logstash_app_url
}

output "node_red_app_url" {
  value = module.stir.node_red_app_url
}

output "thehive_app_url" {
  value = module.stir.thehive_app_url
}
```
---

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13.1 |
| aws | >= 3.22.0 |
| local | >= 2.0 |
| null | >= 3.0 |
| volterra | 0.4.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.22.0 |
| local | >= 2.0 |
| null | >= 3.0 |
| volterra | 0.4.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allow\_dns\_list | List of IP prefixes to be allowed | `list(string)` | <pre>[<br>  "8.8.8.8/32"<br>]</pre> | no |
| allow\_tls\_prefix\_list | Allow TLS prefix list | `list(string)` | <pre>[<br>  "gcr.io",<br>  "storage.googleapis.com",<br>  "docker.io",<br>  "docker.com",<br>  "amazonaws.com"<br>]</pre> | no |
| app\_domain | FQDN for the app. If you have delegated domain `prod.example.com`, then your app\_domain can be `<app_name>.prod.example.com` | `string` | n/a | yes |
| aws\_access\_key | AWS Access Key. Programmable API access key needed for creating the site | `string` | n/a | yes |
| aws\_az | AWS Availability Zone in which the site will be created | `string` | n/a | yes |
| aws\_instance\_type | AWS instance type used for the Volterra site | `string` | `"t3.2xlarge"` | no |
| aws\_region | AWS Region where Site will be created | `string` | n/a | yes |
| aws\_secret\_key | AWS Secret Access Key. Programmable API secret access key needed for creating the site | `string` | n/a | yes |
| aws\_subnet\_ce\_cidr | Map to hold different CE cidr with key as name of subnet | `map(string)` | n/a | yes |
| aws\_subnet\_eks\_cidr | Map to hold different EKS cidr with key as desired AZ on which the subnet should exist | `map(string)` | n/a | yes |
| aws\_vpc\_cidr | AWS VPC CIDR, that will be used to create the vpc while creating the site | `string` | n/a | yes |
| certified\_hardware | Volterra certified hardware used to create Volterra site on AWS | `string` | `"aws-byol-multi-nic-voltmesh"` | no |
| deny\_dns\_list | List of IP prefixes to be denied | `list(string)` | <pre>[<br>  "8.8.4.4/32"<br>]</pre> | no |
| eks\_only | Flag to enable creation of eks cluster only, other volterra objects will be created through Volterra console | `bool` | `false` | no |
| eks\_port\_range | EKS port range to be allowed | `list(string)` | <pre>[<br>  "30000-32767"<br>]</pre> | no |
| enable\_hsts | Flag to enable hsts for HTTPS loadbalancer | `bool` | `false` | no |
| enable\_redirect | Flag to enable http redirect to HTTPS loadbalancer | `bool` | `true` | no |
| js\_cookie\_expiry | Javascript cookie expiry time in seconds | `number` | `3600` | no |
| js\_script\_delay | Javascript challenge delay in miliseconds | `number` | `5000` | no |
| kubeconfig\_output\_path | Ouput file path, where the kubeconfig will be stored | `string` | `"./"` | no |
| site\_disk\_size | Disk size in GiB | `number` | `80` | no |
| skg\_name | SKG Name. Also used as a prefix in names of related resources. | `string` | n/a | yes |
| ssh\_public\_key | SSH Public Key | `string` | `""` | no |
| volterra\_namespace | Volterra app namespace where the object will be created. This cannot be system or shared ns. | `string` | n/a | yes |
| volterra\_namespace\_exists | Flag to create or use existing volterra namespace | `string` | `false` | no |
| volterra\_site\_name | Name of the existing aws vpc site, this is used only when var eks\_only set to true | `string` | `""` | no |
| vpc\_id | Name of the existing vpc id, this is used only when var eks\_only set to true | `string` | `""` | no |
| app\_namespace | Namespace where the app that is protected by F5 XC WAAP resides. Used when integrating with F5 XC WAAP | `string` | n/a | no |
| xc\_waap\_http\_pull | Flag to enable F5 XC WAAP integration | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| kubeconfig\_filename | EKS kubeconfig file name |
| kibana\_app\_url | Domain VIP to access the Kibana deployed on EKS |
| misp\_web\_app\_url | Domain VIP to access MISP deployed on EKS |
| cortex\_app\_url | Domain VIP to access the Cortex deployed on EKS |
| logstash\_app\_url | Domain VIP to access the Logstash deployed on EKS |
| node\_red\_app\_url | Domain VIP to access the Kibana deployed on EKS |
| thehive\_app\_url | Domain VIP to access TheHive deployed on EKS |
